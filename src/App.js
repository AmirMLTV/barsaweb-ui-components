import './App.scss';
import PlanCard from './components/PlanCard';

function App() {
    return (
        <div className="App">
            <PlanCard
                dir="rtl"
                style={{
                  main_color : '#38AEF1',
                  main_shade : 'rgba(56, 174, 241, 0.15)',
                }}
                plan_title="پلن یک"
                select_callback={()=>{console.log('select')}}
                free = {false}
                monthly_price={10}
                yearly_price={40}
                monthly_word = {'ماهانه'}
                yearly_word = {'سالانه'}
                price_symbol = {'تومان'}
                current_plan={false}
                expire_date={'۱۳۹۷/۰۰/۰۰'}
                information={[
                '۲ گیگابایت فضا',
                'ترافیک ماهیانه نامحدود',
                'صفحه سازی پیشرفته',
                'دامنه رایگان',
                'قابلیت ساخت ۱۵ صفحه',
                'مدیریت وبلاگ و ایجاد مطلب'
            ]}></PlanCard>
        </div>
    );
}

export default App;
