import React from 'react'
import './style.scss'
const Button = ({text, callback, type, background_color, text_color}) => (
    <a
        style={{
        backgroundColor: background_color,
        color: text_color
    }}
        onClick={callback}
        className={`${type === 'curve' ? 'curve' : ''}`}>
        {text}
    </a>
)
export default Button;