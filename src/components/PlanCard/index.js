import React from 'react'
import './style.scss'
import plan_icon from './assets/plan_icon.svg'
import tick from './assets/tick.svg'
import Button from '../Button'
const PlanCard = ({dir, plan_title, monthly_price , yearly_price, price_symbol , monthly_word , yearly_word , free , current_plan , expire_date , information, style , select_callback}) => {
    return (
        <div
            className={`plan_card__container ${dir === 'rtl'
            ? 'rtl'
            : 'ltr'}`}>
            <div className="plan_card__top">
                <div className="plan_card__top__start">
                    <span className='plan_card__top__start__title'>{plan_title}</span>
                    <div className="plan_card__top__start__detail__elements">
                        {information && information.map((item, index) => (
                            <div key={index} className="plan_card__top__start__detail__elements__element">
                                <div className="plan_card__top__start__detail__elements__element__checked_image"
                                style={{
                                    backgroundColor: style.main_shade,
                                }} 
                                >
                                    <svg width="8" height="8" viewBox="0 0 9 7" fill={style.main_color} xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.92674 0.692854L2.89503 5.72456L0.851021 3.68035C0.677372 3.5067 0.395679 3.5067 0.222033 3.68035C0.0483859 3.854 0.0483839 4.13569 0.222033 4.30934L2.58054 6.66784C2.75419 6.84149 3.03588 6.84149 3.20953 6.66784L8.55573 1.32164C8.72938 1.14799 8.72938 0.866461 8.55573 0.692814C8.382 0.519088 8.10062 0.519088 7.92682 0.692659L7.92674 0.692854Z" fill="#38AEF1"/>
                                    </svg>


                                </div>
                                <div className="plan_card__top__start__detail__elements__element__text">
                                    <span>{item}</span>
                                </div>
                            </div>
                        ))
}

                    </div>
                </div>
                <div className="plan_card__top__end" style={{
                    backgroundColor: style.main_shade,
                }}>
                    <svg width="48" height="48" viewBox="0 0 62 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M60.4956 34.2507L34.8028 8.55794C33.6535 7.40861 32.1096 6.73551 30.5418 6.71213L21.2073 6.56117C21.5785 7.22265 21.927 8.02407 22.2406 8.90654C22.7861 10.4737 23.204 12.3195 23.309 14.096C23.3206 14.258 23.3206 14.4089 23.3206 14.5599C23.8895 15.2447 24.1914 16.1038 24.1914 17.0096C24.1914 18.0546 23.7852 19.0413 23.042 19.7844C22.3106 20.5158 21.3239 20.9337 20.2789 20.9337C19.2222 20.9337 18.2472 20.5158 17.5041 19.7844C16.761 19.0419 16.3548 18.0545 16.3548 17.0101C16.3548 15.9651 16.761 14.9783 17.5041 14.2353C17.5741 14.1653 17.6434 14.1077 17.7134 14.0494C17.7017 13.9451 17.6901 13.8291 17.6784 13.7125C17.4808 12.087 16.9353 10.2529 16.3315 9.23105L16.2848 9.16108C16.2038 9.03345 16.1222 8.91747 16.0412 8.80086C15.6233 8.18539 15.1471 7.61655 14.6133 7.08276C14.4273 6.89682 14.242 6.72254 14.0561 6.56058C13.0111 6.74652 12.036 7.23372 11.2696 8.00016C10.2595 9.01019 9.71401 10.3571 9.73733 11.7968L9.80731 16.6844L9.84229 18.5068C8.42603 17.8797 7.06752 16.9396 5.90653 15.7786C4.0025 13.8746 2.7488 11.5408 2.40017 9.23064C2.07494 7.04798 2.58611 5.14379 3.85141 3.87847C5.12837 2.61317 7.02075 2.09038 9.20358 2.42723C11.5256 2.77579 13.8475 4.01776 15.7634 5.93359C15.9376 6.11953 16.1236 6.29381 16.2855 6.49077C16.4598 6.67671 16.6224 6.862 16.7734 7.07126C16.936 7.2572 17.0869 7.45416 17.2262 7.66342C17.3772 7.87268 17.5165 8.08129 17.6558 8.30223C17.6674 8.31389 17.6791 8.33722 17.7024 8.34888C17.7951 8.49983 17.8767 8.66245 17.9577 8.83673C18.4799 9.88175 18.9677 11.4256 19.2113 12.9468C19.223 13.0394 19.2463 13.1327 19.2463 13.2254C19.4439 14.5023 19.4556 15.7449 19.1653 16.6734C18.9794 17.2772 19.3163 17.927 19.9318 18.1246C20.5356 18.3106 21.1971 17.962 21.383 17.3582C21.7432 16.1972 21.7776 14.7693 21.6149 13.3297C21.6033 13.2254 21.5916 13.1094 21.5683 13.0045C21.3824 11.5416 20.9761 10.0786 20.5116 8.87101H20.5C20.233 8.17455 19.9311 7.55905 19.6292 7.07114C19.6175 7.04782 19.6059 7.02449 19.5826 7.00117C19.4783 6.83855 19.385 6.6876 19.269 6.53664C18.7235 5.73587 18.0963 4.98109 17.3998 4.28465C15.1478 2.03266 12.3613 0.54642 9.55149 0.128463C6.62563 -0.324407 4.0134 0.43037 2.21436 2.23019C-1.40794 5.85248 -0.491084 12.6675 4.26882 17.4281C5.92937 19.0769 7.84451 20.3072 9.87624 21.0154L9.89956 22.7219L9.96953 27.2849C9.99286 28.8404 10.666 30.3967 11.8153 31.5459L37.5197 57.2386C38.1928 57.9118 39.0869 58.2603 39.981 58.2603C40.8634 58.2603 41.7575 57.9118 42.4423 57.2386L60.4957 39.1736C61.1572 38.5231 61.5174 37.6414 61.5174 36.7123C61.5174 35.7832 61.1572 34.9124 60.4957 34.251L60.4956 34.2507Z" fill={style.main_color}/>
                    </svg>
                </div>
            </div>
            <div className="plan_card__bottom">
                <div className="plan_card__bottom__start">
                    {free ? (
                        <span>رایگان</span>
                    ) : (
                        <>
                        <div className='plan_card__bottom__start mb-1'>
                            <span className='plan_card__bottom__start__price'> {monthly_price} {price_symbol}</span>
                            <span className='plan_card__bottom__divider'> / </span>
                            <span className='plan_card__bottom__start__price__identifier'> {monthly_word} </span>
                        </div>
                        <div className=''>
                            <span className='plan_card__bottom__start__price'> {yearly_price} {price_symbol}</span>
                            <span className='plan_card__bottom__divider'> / </span>
                            <span className='plan_card__bottom__start__price__identifier'> {yearly_word} </span>
                        </div>
                        </>
                    )}
                </div>
                <div className="plan_card__bottom__end">
                    {current_plan ? (
                        <div className='plan_card__bottom__end__current_plan'>
                            <span className='plan_card__bottom__end__current_plan__title' style={{color : style.main_color}}>پلن کنونی</span>
                            <div className='plan_card__bottom__end__current_plan__expire_date'>
                                <span>تاریخ اتمام : </span>
                                <span>{expire_date}</span>
                            </div>
                        </div>
                    ) : (
                        <Button type="curve" background_color={style.main_color} text_color={'#fff'} text="انتخاب" callback={select_callback}></Button>
                    )}
                </div>
            </div>
        </div>
    )
}
export default PlanCard;